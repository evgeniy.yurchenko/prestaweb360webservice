# Prestaweb360 Webservice module

This is an analogue of a regular webservice used in Prestashop.

## Installing:
```
cd [Your prestashop project parent folder]/modules
git clone https://gitlab.com/evgeniy.yurchenko/prestaweb360webservice
``` 
Then go to the admin panel, to the **IMPROVE** tab and install the module called "webservice".


## Settings:

After installing the module, you can configure it by selecting the available entities to which the web service will have access.

Admin panel -> Improve -> Module manager -> 'webservice' -> Configure -> Settings


## Usage:

After installing the module, you can configure it by selecting the available entities to which the web service will have access.

Admin panel -> Improve -> Module manager -> 'webservice' -> Configure -> Settings

Select all entities which you'll use and save configuration.

After configuring the module for yourself, you can use the module along the route: 
```http://localhost:8888/prestaweb_webservice/{$entity}/{$id}``` (PUT,PATCH,POST,GET,DELETE).

$entity: string that takes the name of the entity. For a list see module configuration.
$id: integer. Id of one row.

##### Get params:

### `Filter:`

`?filter[field]=[value]`

`[1|5] `: `OR` operator: list of possible values

`[1,10]`: `Interval` operator: define interval of possible values 

`[John]`: `Literal` value (not case sensitive)

`[Jo]%`: `Begin` operator: fields begins with the value (not case sensitive)

`%[hn]`: `End` operator: fields ends with the value (not case sensitive)

`%[oh]%`: `Contains` operator: fields contains the value (not case sensitive)

`/prestaweb_webservice/customer/?filter[id]=[1|5]`

`/prestaweb_webservice/customer/?filter[id]=[1,10]`

`/prestaweb_webservice/customer/?filter[firstname]=[John]`

`/prestaweb_webservice/manufacturer/?filter[name]=[appl]%`

### `Display:`

`?display=[...fields]`:Returns only the fields specified in this array

OR

`?display=full`: Returns all the fields of the resource

### `Sort:`

`[{fieldname}_{ASC|DESC}]`: The `sort` value is composed of a field name and the expected order separated by a `_`

`/prestaweb_webservice/customer/?sort=[lastname_ASC]`

### `Limit:`
`?limit=1,5`: Either define `offset` and `limit` separated by a `,` (ex: 1,5) or the limit only (`offset` is 0-indexed)

`/prestaweb_webservice/states/?limit=5`: Only include the first 5 `states`

`/prestaweb_webservice/states/?limit=9,5`: Only include the first 5 `states` starting from the 10th element
