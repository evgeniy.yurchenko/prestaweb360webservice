<?php

namespace Webservice\Classes;

use Db;
use DbQuery;
use WebserviceService;

class Product extends WebserviceService
{
    protected function categories($id = null)
    {
        $id = $id != null ? $id : $this->id;

        $db = new DbQuery;
        $db->select('id_category');
        $db->from('category_product', 'cProduct');
        $db->where('cProduct.id_product = ' . $id);

        return Db::getInstance()->executeS($db);
    }

    protected function whole_price($id = null)
    {
        $id = $id != null ? $id : $this->id;

        $db = new DbQuery;
        $db->select('wholesale_price');
        $db->from('product_shop', 'cProduct');
        $db->where('cProduct.id_product = ' . $id);

        $result = Db::getInstance()->executeS($db);
        return empty($result) ? false : $result[0]["wholesale_price"];

    }

    public function update($updateData)
    {
        $categories = $this->updateData['associations']['categories'];
        $product = parent::update($updateData);
        $product->updateCategories($categories);
    }

    public function execute()
    {
        $array = parent::execute();

        if ($this->display == 'full' || !empty($array)) {
            if (!empty($this->id)) {
                $whole_price = $this->whole_price();
                if ($whole_price)
                    $array[0]['wholesale_price'] = $whole_price;
                $array[0]['associations']['category'] = $this->categories();
            } else {
                foreach ($array as &$item) {
                    $whole_price = $this->whole_price($item[$this->getIdEntityField()]);
                    if ($whole_price)
                        $array[0]['wholesale_price'] = $whole_price;
                    $item['associations']['category'] = $this->categories($item[$this->getIdEntityField()]);
                }
            }
        }

        return $array;
    }
}
