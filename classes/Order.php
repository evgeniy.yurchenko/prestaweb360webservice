<?php

namespace Webservice\Classes;

use OrderHistory;
use WebserviceService;
use Db;
use DbQuery;

class Order extends WebserviceService
{
    public function update($updateData)
    {
        $order = parent::update($updateData);
//        var_dump($updateData);die;
        if ($order->current_state != $updateData['data']['current_state']) {
            $orderId = (int)$order->id;

            $history = new OrderHistory;
            $history->id_order = $orderId;
            $history->changeIdOrderState((int)$updateData['data']['current_state'], $order);
            $history->addWithemail(true);
        }
        return $order;
    }

    protected function cDicountPaymentClogistique(?int $idOrder = null)
    {

        $id = $idOrder != null ? $idOrder : $this->id;
        $db = new DbQuery;
        $db->select('clogistique,mp_order_id');
        $db->from('marketplace_orders', 'cOrder');
        $db->where('cOrder.id_order = ' . $id);
        $result = Db::getInstance()->executeS($db);
        if (isset($result[0])) {
            $result[0]["clogistique"] = +$result[0]["clogistique"];
        }
        return empty($result) ? false : $result[0];
    }
//
//    public function execute()
//    {
//        $array = parent::execute();
//
//        foreach ($array as $key => $order) {
//            $cd_res = $this->cDicountPaymentClogistique($order['id_order']);
//
//            if ($cd_res) {
//                $array[$key]["payment"] .= $cd_res["clogistique"] ? " clogistique" : "";
//                $array[$key]["cdiscount_id"] = $cd_res["mp_order_id"];
//            }
//
//        }
//        return $array;
//    }

}
