<?php

namespace Webservice\Classes;

use OrderHistory;
use WebserviceService;
use Db;
use DbQuery;

class FeatureValue extends WebserviceService
{


    protected function getProducts($idFeature){
//        var_dump($idFeature);
        $db = new DbQuery;
        $db->select('fProduct.id_product as product');
        $db->from('feature_product', 'fProduct');
        $db->where('fProduct.id_feature= '.$idFeature );
        $result = Db::getInstance()->executeS($db);
        return $result;
    }

    public function execute()
    {
        $array = parent::execute();

	foreach($array as $key=>$feature){
        $array[$key]['associations']["product"]=$this->getProducts($feature['id_feature']);
	}
        return $array;
    }

}
