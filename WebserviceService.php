<?php

class WebserviceService
{
    public $db;
    public $id_lang;

    public $resource;
    public $id;

    public $filter;
    public $display;
    public $sort;
    public $limit;

    public $updateData;
    public $newData;

    public $entityClass;

    public function execute()
    {
        return Db::getInstance()->executeS($this->db);
    }

    public function showAll()
    {
        if ($this->displayParam($this->display) === 'full') {
            $this->db->select('*');

            if ($this->issetLangTable()) {
                $this->db->leftJoin($this->resource . '_lang', 'langTable',
                    't.' . $this->getIdEntityField() . ' = langTable.' . $this->getIdEntityField()
                    . ' AND langTable.id_lang = ' . $this->id_lang)
                    ->select('t.' . $this->getIdEntityField());
            }

        } elseif (empty($this->displayParam($this->display))) {
            $this->db->select($this->getIdEntityField());
        } else {
            $this->db->select($this->displayParam($this->display));
        }

        $this->db->from($this->resource, 't');

        return $this->db;
    }

    public function showOne()
    {
        $this->display ? $this->db->select($this->displayParam($this->display)) : $this->db->select('*');

        $this->db->from($this->resource, 't');
        $this->db->where('t.' . $this->getIdEntityField() . ' = ' . $this->id);

        if ($this->issetLangTable()) {
            $this->db
                ->leftJoin($this->resource . '_lang', 'langTable',
                    't.' . $this->getIdEntityField() . ' = langTable.' . $this->getIdEntityField()
                    . ' AND langTable.id_lang = ' . $this->id_lang);
        }

        return $this->db;
    }

    public function create($newData)
    {
        $entityFields = $this->entityClass::$definition['fields'];
        $entity = new $this->entityClass();
        foreach ($newData as $key => $value) {
            if (array_key_exists($key, $entityFields)) {
                $entity->$key = $this->getValueType($value, $entityFields[$key]['type']);
            }
        }

        try {
            $entity->save();
        } catch (Exception $exception) {
            http_response_code(500);
            exit($exception->getMessage());
        }
        http_response_code(200);

        return $entity;
    }

    public function update($updateData)
    {
        $entityFields = $this->entityClass::$definition['fields'];

        $entity = new $this->entityClass($this->id, $this->id_lang);

        foreach ($updateData["data"] as $key => $value) {
            if (array_key_exists($key, $entityFields) && property_exists($entity, $key)) {
                if($key=="lastname" || $key=="firstname") $value=preg_replace('/\d+/u', '', $value);
                $entity->$key = $this->getValueType($value, $entityFields[$key]['type']);
            }
        }

        try {
            $entity->save();
            $this->id = $entity->id;
        } catch (Exception $exception) {
//            var_dump($entity);

            http_response_code(500);
            exit($exception->getMessage());
        }
        http_response_code(200);

        return $entity;
    }

    public function remove()
    {
        $entity = (new $this->entityClass($this->id, $this->id_lang));

        try {
            $entity->delete();
        } catch (Exception $exception) {
            http_response_code(500);
            exit($exception->getMessage());
        }
        http_response_code(200);

        return $entity;
    }

    public function arrayToXml($data)
    {
        $xml = new SimpleXMLElement(
            '<!DOCTYPE inline_dtd[<!ENTITY nbsp "&#160;">]>
                    <' . $this->resource . '/>');

        if (!$this->id) {
            foreach ($data as $item) {
                $entity = $xml->addChild($this->resource);

                foreach ($item as $key => $value) {
                    if ($key === $this->getIdEntityField()) $key = 'id';

                    if ($key == 'associations') {
                        $associations = $entity->addChild('associations');

                        foreach ($value as $resource => $resourceArray) {
                            $resourceXml = $associations->addChild($resource);

                            foreach ($resourceArray as $resourceValue) {
                                $resourceXml->addChild($resource, $resourceValue[array_key_first($resourceValue)]);
                            }
                        }
                    }
                    $entity->addChild($key, $value);
                }
            }
        } else {
            foreach ($data as $entity) {
                foreach ($entity as $key => $value) {
                    if ($key == 'associations') {
                        $associations = $xml->addChild('associations');

                        foreach ($value as $resource => $resourceArray) {
                            $resourceXml = $associations->addChild($resource);

                            foreach ($resourceArray as $item) {
                                $resourceXml->addChild($resource, $item[array_key_first($item)]);
                            }
                        }
                    }

                    $xml->addChild($key, $value);
                }
            }
        }

        return $xml->asXML();
    }

    public function method($method)
    {
        switch ($method) {
            case 'GET':
                if ($this->id) {
                    $this->showOne();
                } else {
                    $this->showAll();
                }
                break;
            case 'POST':
                $this->create($this->newData);
                break;
            case 'PUT':
            case 'PATCH':
                $entity = $this->update($this->updateData);
                exit("$entity->id");
                break;
            case 'DELETE':
                $this->remove();
                break;
            default:
                exit('Method not found');
        }

        return Db::getInstance()->executeS($this->db);
    }

    public function params($filter, $display, $sort, $limit)
    {

        if ($display && $this->id) {
            $this->displayParam($display);
        }

        if ($sort && !$this->id) {
            $this->sortParam($sort);
        }

        if ($limit && !$this->id) {
            $this->limitParam($limit);
        }
        if ($filter && !$this->id) {
            $this->filterParam($filter);
        }
    }

    public function filterParam($filter)
    {
        $tmpField = explode(',', array_key_first($filter))[0];

        if ($tmpField === 'id') {
            $field = $this->getIdEntityField();
        } else {
            $field = $tmpField;
        }

        if (strpos($filter[$tmpField], '|')) {
            $sql = $this->orOperator(explode('|', trim($filter[$tmpField], '[]')), $field);
        } elseif (strpos($filter[$tmpField], ',')) {
            $sql = $this->intervalOperator(explode(',', trim($filter[$tmpField], '[]')), $field);
        } else {
            $sql = $this->searchOperator($filter[$tmpField], $field);
        }

        $this->db = trim(implode('', $sql));
    }

    public function orOperator($values, $field)
    {
        if ($this->id != '' || ($this->id != '' && $values)) {
            return [$this->db];
        }

        if (strpos($this->db, 'WHERE')) {
            $sql = explode('WHERE', $this->db);
            $sql[array_key_last($sql)] = ' AND ' . $sql[array_key_last($sql)];
        } else {
            $this->db->where('1');
            $sql = explode('WHERE (1)', $this->db);
        }
        $sql[0] .= 'WHERE';

        $i = 0;
        foreach ($values as $value) {
            if ($i == 0) {
                $sql[0] .= ' (t.' . $field . '=' . $value . ')';
            } else {
                $sql[0] .= ' OR (t.' . $field . '=' . $value . ')';
            }
            $i++;
        }

        return $sql;
    }

    public function intervalOperator($values, $field)
    {
        $from = $values[array_key_first($values)];
        $to = $values[array_key_last($values)];

        return [$this->db->where('t.' . $field . ' BETWEEN ' . $from . ' AND ' . $to)];
    }

    public function searchOperator($value, $field)
    {
        $str = explode('[', $value);
        $str = implode('', array_slice($str, 0, 3));
        $str = explode(']', $str);
        $value = implode('', array_slice($str, 0, 3));

        return [$this->db->where('t.' . $field . ' LIKE \'' . $value . '\'')];
    }

    public function displayParam($display)
    {
        $display = trim($display, '[]');
        $fields = explode(',', $display);
        $tmpFields = [];

        foreach ($fields as $field) {
            if ($field === 'id') {
                $tmpFields[] = $this->getIdEntityField();
            } else {
                $tmpFields[] = $field;
            }
        }

        $tmpFields = implode(',', $tmpFields);

        return $tmpFields;
    }

    public function sortParam($sort)
    {
        $sort = trim($sort, '[]');
        $sort = explode('_', $sort);

        $field = $sort[0];
        $sortType = strtoupper($sort[1]);

        if (count($sort) === 2 && ($sortType === 'ASC' || $sortType === 'DESC')) {
            if ($field === 'id') {
                $field = $this->getIdEntityField();
            }

            $this->db->orderBy('t.' . $field . ' ' . $sortType);
        }
    }

    public function limitParam($limit)
    {
        $limit = explode(',', $limit);
        count($limit) === 1 ? $this->db->limit($limit[0]) : $this->db->limit($limit[0], $limit[1]);
    }

    public function getIdEntityField()
    {
        if ($this->resource === 'orders') {
            return 'id_order';
        }

        return 'id_' . $this->resource;
    }

    public function langTableKeys()
    {
        $db = new DbQuery();
        $db->select('*');
        $db->from($this->resource . '_lang', 't');
        $fields = Db::getInstance()->executeS($db);

        return array_keys($fields[0]);
    }

    public function tableKeys()
    {
        $db = new DbQuery();
        $db->select('*');
        $db->from($this->resource, 't');
        $fields = Db::getInstance()->executeS($db);

        return array_keys($fields[0]);
    }

    public function issetLangTable()
    {
        $db = new DbQuery;
        $db = $db->select($this->getIdEntityField())->from($this->resource . '_lang');

        return Db::getInstance()->executeS($db);
    }

    private function getValueType($value, $type)
    {
        switch ($type) {
            case ObjectModel::TYPE_INT:
                return (int)$value;
            case ObjectModel::TYPE_BOOL:
                return (bool)$value;
            case ObjectModel::TYPE_STRING:
                return (string)$value;
            case ObjectModel::TYPE_FLOAT:
                return (float)$value;
            default:
                return $value;
        }
    }
}
