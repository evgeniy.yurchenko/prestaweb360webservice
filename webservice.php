<?php
/**
 * 2007-2020 PrestaShop
 *
 * NOTICE OF LICENSE
 *
 * This source file is subject to the Academic Free License (AFL 3.0)
 * that is bundled with this package in the file LICENSE.txt.
 * It is also available through the world-wide-web at this URL:
 * http://opensource.org/licenses/afl-3.0.php
 * If you did not receive a copy of the license and are unable to
 * obtain it through the world-wide-web, please send an email
 * to license@prestashop.com so we can send you a copy immediately.
 *
 * DISCLAIMER
 *
 * Do not edit or add to this file if you wish to upgrade PrestaShop to newer
 * versions in the future. If you wish to customize PrestaShop for your
 * needs please refer to http://www.prestashop.com for more information.
 *
 * @author    PrestaShop SA <contact@prestashop.com>
 * @copyright 2007-2020 PrestaShop SA
 * @license   http://opensource.org/licenses/afl-3.0.php  Academic Free License (AFL 3.0)
 *  International Registered Trademark & Property of PrestaShop SA
 */

if (!defined('_PS_VERSION_')) {
    exit;
}

class Webservice extends Module
{
    protected $config_form = false;

    public function __construct()
    {
        $this->name = 'webservice';
        $this->tab = 'administration';
        $this->version = '1.0.0';
        $this->author = 'PrestaWeb360';
        $this->need_instance = 0;

        /**
         * Set $this->bootstrap to true if your module is compliant with bootstrap (PrestaShop 1.6)
         */
        $this->bootstrap = true;

        parent::__construct();

        $this->displayName = $this->l('webservice');
        $this->description = $this->l('This is a webservice from PrestaWeb360');

        $this->ps_versions_compliancy = array('min' => '1.6', 'max' => _PS_VERSION_);
    }

    /**
     * Don't forget to create update methods if needed:
     * http://doc.prestashop.com/display/PS16/Enabling+the+Auto-Update
     */
    public function install()
    {
        Configuration::updateValue('WEBSERVICE_LIVE_MODE', false);

        return parent::install() &&
            $this->registerHook('header') &&
            $this->registerHook('backOfficeHeader') &&
            $this->registerHook('moduleRoutes');;
    }

    public function uninstall()
    {
        Configuration::deleteByName('WEBSERVICE_LIVE_MODE');

        return parent::uninstall();
    }

    /**
     * Load the configuration form
     */
    public function getContent()
    {
        /**
         * If values have been submitted in the form, process.
         */
        if (((bool)Tools::isSubmit('submitWebserviceModule')) == true) {
            $this->postProcess();
        }

        $this->context->smarty->assign('module_dir', $this->_path);

        $output = $this->context->smarty->fetch($this->local_path . 'views/templates/admin/configure.tpl');

        return $output . $this->renderForm();
    }

    /**
     * Create the form that will be displayed in the configuration of your module.
     */
    protected function renderForm()
    {
        $helper = new HelperForm();

        $helper->show_toolbar = false;
        $helper->table = $this->table;
        $helper->module = $this;
        $helper->default_form_language = $this->context->language->id;
        $helper->allow_employee_form_lang = Configuration::get('PS_BO_ALLOW_EMPLOYEE_FORM_LANG', 0);

        $helper->identifier = $this->identifier;
        $helper->submit_action = 'submitWebserviceModule';
        $helper->currentIndex = $this->context->link->getAdminLink('AdminModules', false)
            . '&configure=' . $this->name . '&tab_module=' . $this->tab . '&module_name=' . $this->name;
        $helper->token = Tools::getAdminTokenLite('AdminModules');

        $helper->tpl_vars = array(
            'fields_value' => $this->getConfigFormValues(), /* Add values for your inputs */
            'languages' => $this->context->controller->getLanguages(),
            'id_language' => $this->context->language->id,
        );

        return $helper->generateForm(array($this->getConfigForm()));
    }

    /**
     * Create the structure of your form.
     */
    protected function getConfigForm()
    {
        $fields = array(
            'form' => array(
                'legend' => array(
                    'title' => $this->l('Settings'),
                    'icon' => 'icon-cogs',
                ),
                'input' => [
                    [
                        'type' => 'text',
                        'label' => 'Webservice password',
                        'name' => 'PSWEB_WEBSERVICE_PASSWORD'
                    ]
                ],
                'submit' => array(
                    'title' => $this->l('Save'),
                ),
            ),
        );

        foreach ($this->fields() as $field) {
            $fields["form"]["input"][] = array(
                'type' => 'switch',
                'label' => $this->l($field['resource']),
                'name' => $field['resource'],
                'is_bool' => true,
                'values' => array(
                    array(
                        'id' => 'active_on',
                        'value' => true,
                        'label' => $this->l('Enabled')
                    ),
                    array(
                        'id' => 'active_off',
                        'value' => false,
                        'label' => $this->l('Disabled')
                    )
                ),
            );
        }

        return $fields;
    }

    /**
     * Set values for the inputs.
     */
    protected function getConfigFormValues()
    {
        $values = [];

        foreach ($this->fields() as $field) {
            $values[$field['resource']] = Configuration::get($field['resource']);
        }

        return $values;
    }

    /**
     * Save form data.
     */
    protected function postProcess()
    {
        $password = Tools::getValue('PSWEB_WEBSERVICE_PASSWORD');

        if (!empty($password)) {
            $password = password_hash($password, PASSWORD_DEFAULT);
            Configuration::updateValue('PSWEB_WEBSERVICE_PASSWORD', $password);
        }

        $form_values = $this->getConfigFormValues();

        $fields = json_encode($this->fields());

        Configuration::updateValue('webservice_fields', $fields);

        foreach (array_keys($form_values) as $key) {
            Configuration::updateValue($key, Tools::getValue($key));
        }
    }

    /**
     * Add the CSS & JavaScript files you want to be loaded in the BO.
     */
    public function hookBackOfficeHeader()
    {
        if (Tools::getValue('module_name') == $this->name) {
            $this->context->controller->addJS($this->_path . 'views/js/back.js');
            $this->context->controller->addCSS($this->_path . 'views/css/back.css');
        }
    }

    /**
     * Add the CSS & JavaScript files you want to be added on the FO.
     */
    public function hookHeader()
    {
        $this->context->controller->addJS($this->_path . '/views/js/front.js');
        $this->context->controller->addCSS($this->_path . '/views/css/front.css');
    }

    public function hookModuleRoutes($params)
    {
        return array(
            'module-webservice-webservice' => array(
                'controller' => 'webservice',
                'rule' => 'prestaweb_webservice/{:entity}/{:id}',
                'keywords' => array(
                    'entity' => array('regexp' => '[_a-zA-Z0-9-\pL]*', 'param' => 'entity'),
                    'id' => array('regexp' => '[0-9]*', 'param' => 'id',),
                ),
                'params' => array(
                    'fc' => 'module',
                    'module' => 'webservice',
                ),
            ),

        );
    }

    protected function fields()
    {
        return [
            'addresses' => [
                'resource' => 'address',
                'class' => Address::class
            ],
            'carriers' => [
                'resource' => 'carrier',
                'class' => Carrier::class
            ],
            'cart_rules' => [
                'resource' => 'cart_rule',
                'class' => CartRule::class
            ],
            'carts' => [
                'resource' => 'cart',
                'class' => Cart::class
            ],
            'categories' => [
                'resource' => 'category',
                'class' => Category::class
            ],
            'combinations' => [
                'resource' => 'product_attribute',
                'class' => Combination::class
            ],
            'configurations' => [
                'resource' => 'configuration',
                'class' => Configuration::class
            ],
            'contacts' => [
                'resource' => 'contact',
                'class' => Contact::class
            ],
            'countries' => [
                'resource' => 'country',
                'class' => Country::class
            ],
            'currencies' => [
                'resource' => 'currency',
                'class' => Currency::class
            ],
            'customer_messages' => [
                'resource' => 'customer_message',
                'class' => CustomerMessage::class
            ],
            'customer_threads' => [
                'resource' => 'customer_thread',
                'class' => CustomerThread::class
            ],
            'customer' => [
                'resource' => 'customer',
                'class' => Customer::class
            ],
            'customizations' => [
                'resource' => 'customization',
                'class' => Customization::class
            ],
            'deliveries' => [
                'resource' => 'delivery',
                'class' => Delivery::class
            ],
            'employees' => [
                'resource' => 'employee',
                'class' => Employee::class
            ],
            'groups' => [
                'resource' => 'group',
                'class' => Group::class
            ],
            'guests' => [
                'resource' => 'guests',
                'class' => Guest::class
            ],
            'image_types' => [
                'resource' => 'image_type',
                'class' => ImageType::class
            ],
            'images' => [
                'resource' => 'image',
                'class' => Image::class
            ],
            'languages' => [
                'resource' => 'lang',
                'class' => Language::class
            ],
            'manufacturers' => [
                'resource' => 'manufacturer',
                'class' => Manufacturer::class
            ],
            'messages' => [
                'resource' => 'message',
                'class' => Message::class
            ],
            'order_carriers' => [
                'resource' => 'order_carrier',
                'class' => OrderCarrier::class
            ],
            'order_details' => [
                'resource' => 'order_detail',
                'class' => OrderDetail::class
            ],
            'order_histories' => [
                'resource' => 'order_history',
                'class' => OrderHistory::class
            ],
            'order_invoice' => [
                'resource' => 'order_invoice',
                'class' => OrderInvoice::class
            ],
            'order_payments' => [
                'resource' => 'order_payment',
                'class' => OrderPayment::class
            ],
            'order_slip' => [
                'resource' => 'order_slip',
                'class' => OrderSlip::class
            ],
            'order_states' => [
                'resource' => 'order_state',
                'class' => OrderState::class
            ],
            'orders' => [
                'resource' => 'orders',
                'class' => Order::class
            ],
            'product_suppliers' => [
                'resource' => 'product_supplier',
                'class' => ProductSupplier::class
            ],
            'products' => [
                'resource' => 'product',
                'class' => Product::class
            ],
            'shop_groups' => [
                'resource' => 'shop_group',
                'class' => ShopGroup::class
            ],
            'shop_urls' => [
                'resource' => 'shop_url',
                'class' => ShopUrl::class
            ],
            'shops' => [
                'resource' => 'shop',
                'class' => Shop::class
            ],
            'specific_price_rules' => [
                'resource' => 'specific_price_rule',
                'class' => SpecificPriceRule::class
            ],
            'specific_prices' => [
                'resource' => 'specific_price',
                'class' => SpecificPrice::class
            ],
            'states' => [
                'resource' => 'state',
                'class' => State::class
            ],
            'stock_availables' => [
                'resource' => 'stock_available',
                'class' => StockAvailable::class
            ],
            'stocks' => [
                'resource' => 'stock',
                'class' => Stock::class
            ],
            'stores' => [
                'resource' => 'store',
                'class' => Store::class
            ],
            'suppliers' => [
                'resource' => 'supplier',
                'class' => Supplier::class
            ],
            'supply_order_details' => [
                'resource' => 'supply_order_detail',
                'class' => SupplyOrderDetail::class
            ],
            'supply_order_histories' => [
                'resource' => 'supply_order_history',
                'class' => SupplyOrderHistory::class
            ],
            'supply_order_receipt_histories' => [
                'resource' => 'supply_order_receipt_history',
                'class' => SupplyOrderReceiptHistory::class
            ],
            'supply_order_states' => [
                'resource' => 'supply_order_state',
                'class' => SupplyOrderState::class
            ],
            'supply_orders' => [
                'resource' => 'supply_order',
                'class' => SupplyOrder::class
            ],
            'tax' => [
                'resource' => 'tax',
                'class' => Tax::class
            ],
            'attribute_group' => [
                'resource' => 'attribute_group',
                'class' => AttributeGroup::class
            ],
            'feature' => [
                'resource' => 'feature',
                'class' => Feature::class
            ],
            'tax_rule' => [
                'resource' => 'tax_rule',
                'class' => TaxRule::class
            ],
            'country' => [
                'resource' => 'country',
                'class' => Country::class
            ],
            'feature_value' => [
                'resource' => 'feature_value',
                'class' => FeatureValue::class
            ],
//            'cart_product' => [
//                'resource' => 'cart_product',
//                'class' => CartProduct::class
//            ],
        ];
    }
}
