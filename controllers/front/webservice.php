<?php

require 'modules/webservice/WebserviceService.php';
require 'modules/webservice/classes/Product.php';
require 'modules/webservice/classes/Order.php';
require 'modules/webservice/classes/FeatureValue.php';
use Webservice\Classes\FeatureValue;
use Webservice\Classes\Product;
use Webservice\Classes\Order;

class WebserviceWebserviceModuleFrontController extends ModuleFrontController
{
    private $id;
    private $resource;
    protected $ws;

    public function __construct()
    {
        parent::__construct();

        $this->id = empty($this->ws->id) ? null : $this->ws->id;

        global $cookie;

        $this->resource = Tools::getValue('entity');


        if ($this->resource == 'product') {
            $this->ws = new Product;
        } elseif ($this->resource == 'orders') {
            $this->ws = new Order;
        } elseif ($this->resource == 'feature_value') {
            $this->ws = new FeatureValue;
        } else {
            $this->ws = new WebserviceService;
        }

        $this->ws->id_lang = $cookie->id_lang;

        $this->ws->db = new DbQuery;

        $this->resource = Tools::getValue('entity');

        $this->ws->resource = $this->resource;

        $this->ws->id = Tools::getValue('id');

        $this->ws->filter = Tools::getValue('filter');
        $this->ws->display = Tools::getValue('display');
        $this->ws->sort = Tools::getValue('sort');
        $this->ws->limit = Tools::getValue('limit');

        parse_str(file_get_contents('php://input'), $updateData);

        $this->ws->updateData = $updateData;
        $this->ws->newData = $_POST;

        $fields = json_decode(Configuration::get('webservice_fields'), true);
        foreach ($fields as $field) {
            if ($field['resource'] == $this->resource) {
                $this->ws->entityClass = $field['class'];
            }
        }
    }

    public function initContent()
    {
        $configurationPassword = Configuration::get('PSWEB_WEBSERVICE_PASSWORD');

        if (!isset($_SERVER['PHP_AUTH_USER'])) {
            header('WWW-Authenticate: Basic realm="My Realm"');
            header('HTTP/1.0 401 Unauthorized');
            exit;
        } else {
            $userPassword = $_SERVER['PHP_AUTH_PW'];

            if (password_verify($userPassword, $configurationPassword)) {

                if (!Configuration::get($this->ws->resource) || (int)Configuration::get($this->ws->resource) != 1) {
                    exit('Entity not found or disable');
                }

                $this->ws->method($_SERVER['REQUEST_METHOD']);

                $this->ws->params($this->ws->filter, $this->ws->display, $this->ws->sort, $this->ws->limit);

                $execute = $this->ws->execute();

                if (count($execute[0]) > 0) {
                    header("Content-Type: application/xml");
                    echo $this->ws->arrayToXml($execute);
                } else {
                    header("HTTP/1.0 404 Not Found");
                    exit('');
                }
            } else {
                unset($_SERVER['PHP_AUTH_USER']);
                header('WWW-Authenticate: Basic realm="My Realm"');
            }
        }
    }
}
